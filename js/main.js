$(function(){
    $(window).scroll(function(){
        var scroll = $(window).scrollTop();

        if(scroll > 100)
        {
            $('#back-to-top').removeClass("hide").addClass("show");
        }
        else
        {
            $('#back-to-top').removeClass("show").addClass("hide");
        }
    })
})